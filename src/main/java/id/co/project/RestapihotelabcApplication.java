package id.co.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class RestapihotelabcApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(RestapihotelabcApplication.class, args);
    }
}
