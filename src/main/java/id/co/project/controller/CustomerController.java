package id.co.project.controller;

import id.co.project.dto.ParamCustomer;
import id.co.project.dto.ParamGetCustomer;
import id.co.project.dto.ResponeOut;
import id.co.project.model.TblCustomer;
import id.co.project.service.TblCustomerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomerController {

    @Autowired
    private TblCustomerService custService;

    @RequestMapping(value = "/customers/save", method = RequestMethod.POST)
    @ApiOperation(value = "Add New Or Update Customers", notes = "Add New Or Update Customers")
    public ResponseEntity<ResponeOut> SaveOrUpdateCustomer(ParamCustomer param) {
        ResponeOut out = new ResponeOut();
        int update = 0;
        int save = 0;
        try {
            if (param.getIdCust() == null || param.getIdCust().equals("") || param.getIdCust().isEmpty()) {
                out.setResponeCode("400");
                out.setResponeMessage("idCust is mandatory");
                return ResponseEntity.ok(out);
            }
            if (param.getNamaCust() == null || param.getNamaCust().equals("") || param.getNamaCust().isEmpty()) {
                out.setResponeCode("400");
                out.setResponeMessage("namaCust is mandatory");
                return ResponseEntity.ok(out);
            }
            if (param.getNoIdentityCust() == null || param.getNoIdentityCust().equals("") || param.getNoIdentityCust().isEmpty()) {
                out.setResponeCode("400");
                out.setResponeMessage("noIdentityCust is mandatory");
                return ResponseEntity.ok(out);
            }
            if (param.getAlamatCust() == null || param.getAlamatCust().equals("") || param.getAlamatCust().isEmpty()) {
                out.setResponeCode("400");
                out.setResponeMessage("alamatCust is mandatory");
                return ResponseEntity.ok(out);
            }
            TblCustomer cust = custService.getDataCustById(param.getIdCust());
            if (cust != null) {
                update = custService.updateCust(param.getIdCust(), param.getNamaCust(), param.getNoIdentityCust(), param.getAlamatCust());
            } else {
                save = custService.saveCust(param.getIdCust(), param.getNamaCust(), param.getNoIdentityCust(), param.getAlamatCust());
            }
            if (update == 1 || save == 1) {
                out.setResponeCode("200");
                out.setResponeMessage("Data Customer id : " + param.getIdCust() + " Updated");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(out);
    }

    @RequestMapping(value = "customers/getData", method = RequestMethod.GET)
    @ApiOperation(value = "Get Data Customers", notes = "Get Data Customers")
    public List<TblCustomer> getDataCustomer(ParamGetCustomer param) {
        List<TblCustomer> custList = null;
        try {
            if (param.getParam().equals("all")) {
                custList = custService.getDataCustAll();
            } else if (param.getParam().equals("criteria")) {
                if (param.getIdCust() != null || param.getNoIdentityCust() != null){
                    custList = custService.getDataCustByParamMatch(param.getIdCust(), param.getNoIdentityCust());
                }else if (param.getNamaCust() != null) {
                    custList = custService.getDataCustByNama(param.getNamaCust());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return custList;
    }

    @ApiOperation(value = "Delete Data Customers", notes = "Delete Data Customers")
    @RequestMapping(value = "customers/delete", method = RequestMethod.DELETE)
    public ResponseEntity<ResponeOut> deleteCustomer(@RequestParam String idCust) {
        ResponeOut out = new ResponeOut();
        int delete = 0;
        try {
            if (idCust == null || idCust.equals("") || idCust.isEmpty()) {
                out.setResponeCode("400");
                out.setResponeMessage("idCust is mandatory");
                return ResponseEntity.ok(out);
            }
            delete = custService.deleteCust(idCust);
            if (delete == 1) {
                out.setResponeCode("200");
                out.setResponeMessage("Data Customer Id : " + idCust + " Removed");
            } else {
                out.setResponeCode("400");
                out.setResponeMessage("Data Customer Id : " + idCust + " Not Exist");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(out);
    }

}
