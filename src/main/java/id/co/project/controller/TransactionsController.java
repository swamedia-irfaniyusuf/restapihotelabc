package id.co.project.controller;

import id.co.project.dto.ParamCheckoutTransacion;
import id.co.project.dto.ParamGetTransacion;
import id.co.project.dto.ParamTransacion;
import id.co.project.dto.ResponeOut;
import id.co.project.model.TblCustomer;
import id.co.project.model.TblRoom;
import id.co.project.model.TblTransaction;
import id.co.project.service.TblCustomerService;
import id.co.project.service.TblRoomService;
import id.co.project.service.TblTransactionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class TransactionsController {

    @Autowired
    private TblCustomerService custService;

    @Autowired
    private TblRoomService roomService;

    @Autowired
    private TblTransactionService trxService;

    @RequestMapping(value = "transactions/checkin", method = RequestMethod.POST)
    @ApiOperation(value = "Checkin Data Hotels", notes = "Checkin Data Hotels")
    public ResponseEntity<ResponeOut> registration(@RequestBody ParamTransacion param) {
        ResponeOut out = new ResponeOut();
        int result = 0;
        int totalTarif = 0;
        try {
            TblTransaction trx = trxService.getDataTransactionByIdTrx(param.getIdTrx());
            if (trx == null) {
                TblTransaction trxRoom = trxService.getDataTransactionByIdRoom(param.getIdRoom());
                if (trxRoom != null) {
                    out.setResponeCode("400");
                    out.setResponeMessage("Room Id : " + param.getIdRoom() + " Not Available");
                    return ResponseEntity.ok(out);
                }
                TblCustomer cust = custService.getDataCustById(param.getIdCust());
                if (cust == null) {
                    out.setResponeCode("400");
                    out.setResponeMessage("Cust Id : " + param.getIdCust() + " Not Exists");
                    return ResponseEntity.ok(out);
                }
                TblRoom room = roomService.getDataRoomById(param.getIdRoom());
                totalTarif = room.getTarif() * param.getLamaMenginap();
                result = trxService.saveTrx(param.getIdTrx(), param.getIdCust(), param.getIdRoom(), param.getTanggalDaftar(), param.getLamaMenginap(), totalTarif);
                if (result == 1) {
                    out.setResponeCode("200");
                    out.setResponeMessage("Transaction Id : " + param.getIdTrx() + " Success");
                }
            } else {
                out.setResponeCode("400");
                out.setResponeMessage("Data Transaction Id : " + param.getIdTrx() + " Exists");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(out);
    }

    @ApiOperation(value = "Checkout Data Hotels", notes = "Checkout Data Hotels")
    @RequestMapping(value = "transactions/checkout", method = RequestMethod.POST)
    public ResponseEntity<ResponeOut> checkOut(ParamCheckoutTransacion param) {
        ResponeOut out = new ResponeOut();
        int result = 0;
        try {
            if (param.getParam().equals("trx")) {
                TblTransaction trx = trxService.getDataTransactionByIdTrx(param.getIdTrx());
                if (trx != null) {
                    result = trxService.checkout(param.getIdTrx());
                    if (result == 1) {
                        out.setResponeCode("200");
                        out.setResponeMessage("Transaction Id : " + param.getIdTrx() + " Have Done Checkout");
                    }
                } else {
                    out.setResponeCode("400");
                    out.setResponeMessage("Transaction Id : " + param.getIdTrx() + " Not Exists");
                }

            } else if (param.getParam().equals("cust")) {
                List<TblTransaction> trx2 = trxService.getDataTransactionByIdCust(param.getIdCust());
                if (trx2.size() > 0) {
                    result = trxService.checkoutbyCust(param.getIdCust());
                    if (result != 0) {
                        out.setResponeCode("200");
                        out.setResponeMessage("Customer Id : " + param.getIdCust() + " Have Done Checkout");
                    }
                } else {
                    out.setResponeCode("400");
                    out.setResponeMessage("Customer Id : " + param.getIdCust() + " Not Exists");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(out);
    }

    @ApiOperation(value = "Get Data Transactions", notes = "Get Data Transactions")
    @RequestMapping(value = "transactions/getData", method = RequestMethod.GET)
    public List<TblTransaction> getDataTransaction(ParamGetTransacion param) {
        List<TblTransaction> trans = new ArrayList<>();
        try {
            if (param.getParam().equals("all")) {
                trans = trxService.getDataTransactionAll();
            } else if (param.getParam().equals("criteria")) {
                if (param.getIdTrx() != null) {
                    TblTransaction trx = trxService.getDataTransactionByIdTrx(param.getIdTrx());
                    trans.add(trx);
                } else if (param.getIdCust() != null) {
                    trans = trxService.getDataTransactionByIdCust(param.getIdCust());
                } else if (param.getIdRoom() != null) {
                    TblTransaction trx2 = trxService.getDataTransactionByIdRoom(param.getIdRoom());
                    trans.add(trx2);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return trans;
    }


}
