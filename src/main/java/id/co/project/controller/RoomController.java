package id.co.project.controller;

import id.co.project.dto.ParamGetRoom;
import id.co.project.dto.ParamRoom;
import id.co.project.dto.ResponeOut;
import id.co.project.model.TblRoom;
import id.co.project.service.TblRoomService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RoomController {

    @Autowired
    private TblRoomService roomService;

    @ApiOperation(value = "Add New Or Update Data Rooms", notes = "Add New Or Update Data Rooms")
    @RequestMapping(value = "rooms/save", method = RequestMethod.POST)
    public ResponseEntity<ResponeOut> saveOrUpdateRoom(@RequestBody ParamRoom param) {
        ResponeOut out = new ResponeOut();
        int save = 0;
        int update = 0;
        if (param.getIdRoom() == null || param.getIdRoom().equals("") || param.getIdRoom().isEmpty()) {
            out.setResponeCode("400");
            out.setResponeMessage("idRoom is mandatory");
            return ResponseEntity.ok(out);
        }
        if (param.getTypeRoom() == null || param.getTypeRoom().equals("") || param.getTypeRoom().isEmpty()) {
            out.setResponeCode("400");
            out.setResponeMessage("typeRoom is mandatory");
            return ResponseEntity.ok(out);
        }
        if (param.getTarif() == 0) {
            out.setResponeCode("400");
            out.setResponeMessage("tarif is mandatory");
            return ResponseEntity.ok(out);
        }
        try {
            TblRoom room = roomService.getDataRoomById(param.getIdRoom());
            if (room != null) {
                update = roomService.updateRoom(param.getIdRoom(), param.getTypeRoom(), param.getTarif());
            } else {
                save = roomService.saveRoom(param.getIdRoom(), param.getTypeRoom(), param.getTarif());
            }
            if (update == 1 || save == 1) {
                out.setResponeCode("200");
                out.setResponeMessage("Data Room id : " + param.getIdRoom() + " Updated");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(out);
    }


    @RequestMapping(value = "rooms/getData", method = RequestMethod.GET)
    @ApiOperation(value = "Get Data Rooms", notes = "Get Data Rooms")
    public List<TblRoom> getDataRoom(ParamGetRoom param) {
        List<TblRoom> room = null;
        try {
            if (param.getParam().equals("all")) {
                room = roomService.getDataRoomAll();
            } else if (param.getParam().equals("criteria")) {
                room = roomService.getDataRoomByParam(param.getIdRoom(), param.getTypeRoom());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return room;
    }

    @RequestMapping(value = "rooms/delete", method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete Data Rooms", notes = "Delete Data Rooms")
    public ResponseEntity<ResponeOut> deleteRoom(@RequestParam @Param("idRoom") String idRoom) {
        ResponeOut out = new ResponeOut();
        int delete = 0;
        if (idRoom == null || idRoom.equals("") || idRoom.isEmpty()) {
            out.setResponeCode("400");
            out.setResponeMessage("idRoom is mandatory");
            return ResponseEntity.ok(out);
        }
        try {
            delete = roomService.deleteRoom(idRoom);
            if (delete == 1) {
                out.setResponeCode("200");
                out.setResponeMessage("Data Room Id : " + idRoom + " Removed");
            } else {
                out.setResponeCode("400");
                out.setResponeMessage("Data Room Id : " + idRoom + " Not Exist");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(out);
    }

    @ApiOperation(value = "Get Available Data Rooms", notes = "Get Available Data Rooms")
    @RequestMapping(value = "rooms/available-room", method = RequestMethod.GET)
    public List<TblRoom> getRoomAvailable() {
        List<TblRoom> rooms = null;
        try {
            rooms = roomService.getDataTransactionByAvailableRooms();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rooms;
    }


}
