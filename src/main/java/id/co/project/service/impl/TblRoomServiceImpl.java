package id.co.project.service.impl;

import id.co.project.model.TblRoom;
import id.co.project.repository.TblRoomRepository;
import id.co.project.service.TblRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

@Service("tblRoomServiceImpl")
@Transactional
public class TblRoomServiceImpl implements TblRoomService {

    @Autowired
    private TblRoomRepository roomRepo;

    @Override
    public int saveRoom(String idRoom, String typeRoom, int tarif) {
        int i = roomRepo.saveRoom(idRoom, typeRoom, tarif);
        return i;
    }

    @Override
    public int updateRoom(String idRoom, String typeRoom, int tarif) {
        int y = roomRepo.updateRoom(idRoom, typeRoom, tarif);
        return y ;
    }

    @Override
    public int deleteRoom(String idRoom) {
        int z = roomRepo.deleteRoom(idRoom);
        return z ;
    }

    @Override
    public List<TblRoom> getDataRoomAll() {
        return roomRepo.getDataRoomAll();
    }

    @Override
    public TblRoom getDataRoomById(String idRoom) {
        return roomRepo.getDataRoomById(idRoom);
    }

    @Override
    public List<TblRoom> getDataRoomByParam(String idRoom, String typeRoom) {
        return roomRepo.getDataRoomByParam(idRoom, typeRoom);
    }

    @Override
    public List<TblRoom> getDataTransactionByAvailableRooms() {
        return roomRepo.getDataTransactionByAvailableRooms();
    }
}
