package id.co.project.service.impl;

import id.co.project.model.TblCustomer;
import id.co.project.repository.TblCustomerRepository;
import id.co.project.service.TblCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("tblCustomerServiceImpl")
@Transactional
public class TblCustomerServiceImpl implements TblCustomerService {

    @Autowired
    private TblCustomerRepository custRepo;

    @Override
    public int saveCust(String idCust, String namaCust, String noIdentityCust, String alamat) {
        int j = custRepo.saveCust(idCust, namaCust, noIdentityCust, alamat);
        return j;
    }

    @Override
    public int updateCust(String idCust, String namaCust, String noIdentityCust, String alamat) {
         int i = custRepo.updateCust(idCust, namaCust, noIdentityCust, alamat);
         return i;
    }

    @Override
    public int deleteCust(String idCust) {
        int y =  custRepo.deleteCust(idCust);
        return y ;
    }

    @Override
    public List<TblCustomer> getDataCustAll() {
        return custRepo.getDataCustAll();
    }

    @Override
    public TblCustomer getDataCustById(String idCust) {
        return custRepo.getDataCustById(idCust);
    }

    @Override
    public List<TblCustomer> getDataCustByParamMatch(String idCust, String noIdentityCust) {
        return custRepo.getDataCustByParamMatch(idCust, noIdentityCust);
    }

    @Override
    public List<TblCustomer> getDataCustByNama(String namaCust) {
        return custRepo.getDataCustByNama(namaCust);
    }


}
