package id.co.project.service.impl;

import id.co.project.model.TblRoom;
import id.co.project.model.TblTransaction;
import id.co.project.repository.TblTransactionRepository;
import id.co.project.service.TblTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service("tblTransactionServiceImpl")
@Transactional
public class TblTransactionServiceImpl implements TblTransactionService {

    @Autowired
    private TblTransactionRepository trxRepo;

    @Override
    public int saveTrx(String idTrx, String idCust, String idRoom, Date tanggalDaftar, int lamaMenginap, int totalTarif) {
        int i = trxRepo.saveTrx(idTrx, idCust, idRoom, tanggalDaftar, lamaMenginap, totalTarif);
        return i;
    }

    @Override
    public int checkout(String idTrx) {
        int i = trxRepo.checkout(idTrx);
        return i;
    }

    @Override
    public int checkoutbyCust(String idCust) {
        int i = trxRepo.checkoutByCust(idCust);
        return i ;
    }

    @Override
    public List<TblTransaction> getDataTransactionAll() {
        return trxRepo.getDataTransactionAll();
    }

    @Override
    public TblTransaction getDataTransactionByIdTrx(String idTrx) {
        return trxRepo.getDataTransactionByIdTrx(idTrx);
    }

    @Override
    public TblTransaction getDataTransactionByIdRoom(String idRoom) {
        return trxRepo.getDataTransactionByIdRoom(idRoom);
    }

    @Override
    public List<TblTransaction> getDataTransactionByIdCust(String idCust) {
        return trxRepo.getDataTransactionByIdCust(idCust);
    }


}
