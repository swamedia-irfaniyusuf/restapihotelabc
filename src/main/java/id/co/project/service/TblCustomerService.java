package id.co.project.service;

import id.co.project.model.TblCustomer;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("tblCustomerService")
public interface TblCustomerService {

    public int saveCust(String idCust, String namaCust, String noIdentityCust, String alamat);

    public int updateCust(String idCust, String namaCust, String noIdentityCust, String alamat);

    public int deleteCust(String idCust);

    public List<TblCustomer> getDataCustAll();

    public TblCustomer getDataCustById(String idCust);

    public List<TblCustomer> getDataCustByParamMatch(String idCust, String noIdentityCust);

    public List<TblCustomer> getDataCustByNama(String namaCust);


}
