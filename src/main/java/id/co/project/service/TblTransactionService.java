package id.co.project.service;

import id.co.project.model.TblRoom;
import id.co.project.model.TblTransaction;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("tblTransactionService")
public interface TblTransactionService {

    public int saveTrx(String idTrx,
                       String idCust,
                       String idRoom,
                       Date tanggalDaftar,
                       int lamaMenginap,
                       int totalTarif);

    public int checkout(String idTrx);

    public int checkoutbyCust(String idCust);

    public List<TblTransaction> getDataTransactionAll();

    public TblTransaction getDataTransactionByIdTrx(String idTrx);

    public TblTransaction getDataTransactionByIdRoom(String idRoom);

    public List<TblTransaction> getDataTransactionByIdCust(String idCust);

}
