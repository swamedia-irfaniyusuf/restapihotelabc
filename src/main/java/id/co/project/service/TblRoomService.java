package id.co.project.service;

import id.co.project.model.TblRoom;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("tblRoomService")
public interface TblRoomService {

    public int saveRoom(String idRoom, String typeRoom, int tarif);

    public int updateRoom(String idRoom, String typeRoom, int tarif);

    public int deleteRoom(String idRoom);

    public List<TblRoom> getDataRoomAll();

    public TblRoom getDataRoomById(String idRoom);

    public List<TblRoom> getDataRoomByParam(String idRoom, String typeRoom);

    public List<TblRoom> getDataTransactionByAvailableRooms();
}
