package id.co.project.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ParamTransacion {

    protected String idTrx;
    protected String idRoom;
    protected String idCust;
    protected Date tanggalDaftar;
    protected int lamaMenginap;

    public ParamTransacion(String idTrx, String idRoom, String idCust, Date tanggalDaftar, int lamaMenginap) {
        this.idTrx = idTrx;
        this.idRoom = idRoom;
        this.idCust = idCust;
        this.tanggalDaftar = tanggalDaftar;
        this.lamaMenginap = lamaMenginap;
    }

    public ParamTransacion() {


    }
}
