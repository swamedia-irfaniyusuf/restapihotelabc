package id.co.project.dto;

import lombok.Data;

@Data
public class ParamRoom {

    protected String idRoom;
    protected String typeRoom;
    protected int tarif;

    public ParamRoom() {
    }

    public ParamRoom(String idRoom, String typeRoom, int tarif) {
        this.idRoom = idRoom;
        this.typeRoom = typeRoom;
        this.tarif = tarif;
    }
}
