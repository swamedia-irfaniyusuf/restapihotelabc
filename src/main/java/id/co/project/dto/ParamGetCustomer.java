package id.co.project.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ParamGetCustomer {

    @ApiModelProperty(required=true, value="all / criteria", example = "all")
    protected String param;
    @ApiModelProperty(value="idCust")
    protected String idCust;
    @ApiModelProperty(value="namaCust")
    protected String namaCust;
    @ApiModelProperty(value="noIdentityCust")
    protected String noIdentityCust;


    public ParamGetCustomer() {
    }

    public ParamGetCustomer(String param, String idCust, String namaCust, String noIdentityCust) {
        this.param = param;
        this.idCust = idCust;
        this.namaCust = namaCust;
        this.noIdentityCust = noIdentityCust;
    }
}
