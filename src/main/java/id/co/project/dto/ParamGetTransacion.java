package id.co.project.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class ParamGetTransacion {

    @ApiModelProperty(required=true, value="all / criteria", example = "all")
    protected String param;
    @ApiModelProperty(value="idTrx")
    protected String idTrx;
    @ApiModelProperty(value="idCust")
    protected String idCust;
    @ApiModelProperty(value="idRoom")
    protected String idRoom;

    public ParamGetTransacion() {
    }

    public ParamGetTransacion(String param, String idTrx, String idCust, String idRoom) {
        this.param = param;
        this.idTrx = idTrx;
        this.idCust = idCust;
        this.idRoom = idRoom;
    }
}
