package id.co.project.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ParamGetRoom {

    @ApiModelProperty(required=true, value="all / criteria", example = "all")
    protected String param;
    @ApiModelProperty(value="idRoom")
    protected String idRoom;
    @ApiModelProperty(value="typeRoom")
    protected String typeRoom;

    public ParamGetRoom() {
    }

    public ParamGetRoom(String param, String idRoom, String typeRoom) {
        this.param = param;
        this.idRoom = idRoom;
        this.typeRoom = typeRoom;
    }
}
