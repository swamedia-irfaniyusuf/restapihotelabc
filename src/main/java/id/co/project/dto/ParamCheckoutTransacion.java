package id.co.project.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ParamCheckoutTransacion {

    @ApiModelProperty(required=true, value="trx / cust", example = "trx")
    protected String param;
    @ApiModelProperty(value="idTrx")
    protected String idTrx;
    @ApiModelProperty(value="idCust")
    protected String idCust;

    public ParamCheckoutTransacion() {
    }

    public ParamCheckoutTransacion(String param, String idTrx, String idCust) {
        this.param = param;
        this.idTrx = idTrx;
        this.idCust = idCust;
    }
}
