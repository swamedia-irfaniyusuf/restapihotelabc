package id.co.project.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ParamCustomer {

    @ApiModelProperty(required=true, value="idCust")
    protected String idCust;
    @ApiModelProperty(required=true, value="namaCust")
    protected String namaCust;
    @ApiModelProperty(required=true, value="noIdentityCust")
    protected String noIdentityCust;
    @ApiModelProperty(required=true, value="alamatCust")
    protected String alamatCust;

    public ParamCustomer() {
    }

    public ParamCustomer(String idCust, String namaCust, String noIdentityCust, String alamatCust) {
        this.idCust = idCust;
        this.namaCust = namaCust;
        this.noIdentityCust = noIdentityCust;
        this.alamatCust = alamatCust;
    }
}
