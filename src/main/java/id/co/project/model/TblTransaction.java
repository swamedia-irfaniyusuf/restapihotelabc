package id.co.project.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name = "TBL_TRANSACTION")
public class TblTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_TRX")
    protected String idTrx;
    @Column(name = "ID_CUST")
    protected String idCust;
    @Column(name = "ID_ROOM")
    protected String idRoom;
    @Column(name = "TANGGAL_DAFTAR")
    protected Date tangalDaftar;
    @Column(name = "LAMA_MENGINAP")
    protected int lamaMenginap;
    @Column(name = "TOTAL_TARIF")
    protected int totalTarif;
    @Column(name = "STATUS")
    protected String status;
    @Column(name = "CREATED_DATE")
    protected Date createdDate;
    @Column(name = "MODIFIED_DATE")
    protected Date modifiedDate;

    public TblTransaction() {
    }

    public TblTransaction(String idTrx, String idCust, String idRoom, Date tangalDaftar, int lamaMenginap, int totalTarif, String status, Date createdDate, Date modifiedDate) {
        this.idTrx = idTrx;
        this.idCust = idCust;
        this.idRoom = idRoom;
        this.tangalDaftar = tangalDaftar;
        this.lamaMenginap = lamaMenginap;
        this.totalTarif = totalTarif;
        this.status = status;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
    }
}
