package id.co.project.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name = "TBL_CUSTOMER")
public class TblCustomer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_CUST")
    protected String idCust;
    @Column(name = "NAMA_CUST")
    protected String namaCust;
    @Column(name = "NO_IDENTITY_CUST")
    protected String noIdentityCust;
    @Column(name = "ALAMAT_CUST")
    protected String alamatCust;
    @Column(name = "CREATED_DATE")
    protected Date createdDate;
    @Column(name = "MODIFIED_DATE")
    protected Date modifiedDate;

    public TblCustomer() {
    }

    public TblCustomer(String idCust, String namaCust, String noIdentityCust, String alamatCust, Date createdDate, Date modifiedDate) {
        this.idCust = idCust;
        this.namaCust = namaCust;
        this.noIdentityCust = noIdentityCust;
        this.alamatCust = alamatCust;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
    }
}
