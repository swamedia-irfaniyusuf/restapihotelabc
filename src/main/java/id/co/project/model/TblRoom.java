package id.co.project.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name = "TBL_ROOM")
public class TblRoom implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_ROOM")
    protected String idRoom;
    @Column(name = "TYPE_ROOM")
    protected String typeRoom;
    @Column(name = "TARIF")
    protected int tarif;
    @Column(name = "CREATED_DATE")
    protected Date createdDate;
    @Column(name = "MODIFIED_DATE")
    protected Date modifiedDate;

    public TblRoom() {
    }

    public TblRoom(String idRoom, String typeRoom, int tarif, Date createdDate, Date modifiedDate) {
        this.idRoom = idRoom;
        this.typeRoom = typeRoom;
        this.tarif = tarif;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
    }
}
