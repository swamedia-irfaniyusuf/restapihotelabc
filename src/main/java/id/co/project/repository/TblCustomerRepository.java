package id.co.project.repository;

import id.co.project.model.TblCustomer;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface TblCustomerRepository extends CrudRepository<TblCustomer, String> {

    @Modifying
    @Query(value = "INSERT INTO TBL_CUSTOMER (ID_CUST,NAMA_CUST,NO_IDENTITY_CUST,ALAMAT_CUST,CREATED_DATE) VALUES (:idCust, :namaCust, :noIdentityCust, :alamatCust, sysdate) ", nativeQuery = true)
    public int saveCust(@Param("idCust") String idCust, @Param("namaCust") String namaCust, @Param("noIdentityCust") String noIdentityCust, @Param("alamatCust") String alamat);

    @Modifying
    @Query(value = "UPDATE TBL_CUSTOMER SET NAMA_CUST = :namaCust, NO_IDENTITY_CUST = :noIdentityCust, ALAMAT_CUST = :alamat, MODIFIED_DATE = sysdate WHERE ID_CUST = :idCust ", nativeQuery = true)
    public int updateCust(@Param("idCust") String idCust, @Param("namaCust") String namaCust, @Param("noIdentityCust") String noIdentityCust, @Param("alamat") String alamat);

    @Modifying
    @Query(value = "DELETE FROM TBL_CUSTOMER WHERE ID_CUST = ?1 ", nativeQuery = true)
    public int deleteCust(String idCust);

    @Query(value = "SELECT * FROM TBL_CUSTOMER", nativeQuery = true)
    public List<TblCustomer> getDataCustAll();

    @Query(value = "SELECT * FROM TBL_CUSTOMER WHERE ID_CUST = ?1", nativeQuery = true)
    public TblCustomer getDataCustById(String idCust);

    @Query(value = "SELECT * FROM TBL_CUSTOMER WHERE NAMA_CUST = ?1", nativeQuery = true)
    public List<TblCustomer> getDataCustByNama(String namaCust);

    @Query(value = "SELECT * FROM TBL_CUSTOMER WHERE ID_CUST = ?1 OR NO_IDENTITY_CUST = ?2", nativeQuery = true)
    public List<TblCustomer> getDataCustByParamMatch(String idCust, String noIdentityCust);

}
