package id.co.project.repository;

import id.co.project.model.TblRoom;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TblRoomRepository extends CrudRepository<TblRoom, String> {

    @Modifying
    @Query(value = "INSERT INTO TBL_ROOM (ID_ROOM, TYPE_ROOM, TARIF, CREATED_DATE) VALUES (:idRoom, :typeRoom, :tarif, SYSDATE) ", nativeQuery = true)
    public int saveRoom(@Param("idRoom") String idRoom, @Param("typeRoom") String typeRoom, @Param("tarif") int tarif);

    @Modifying
    @Query(value = "UPDATE TBL_ROOM SET TYPE_ROOM = :typeRoom, TARIF = :tarif, MODIFIED_DATE = SYSDATE WHERE ID_ROOM = :idRoom ",nativeQuery = true)
    public int updateRoom(@Param("idRoom") String idRoom, @Param("typeRoom") String typeRoom, @Param("tarif") int tarif);

    @Modifying
    @Query(value = "DELETE FROM TBL_ROOM WHERE ID_ROOM = ?1 ",nativeQuery = true)
    public int deleteRoom(String idRoom);

    @Query(value = "SELECT * FROM TBL_ROOM ",nativeQuery = true)
    public List<TblRoom> getDataRoomAll();

    @Query(value = "SELECT * FROM TBL_ROOM WHERE ID_ROOM = ?1 ",nativeQuery = true)
    public TblRoom getDataRoomById(String idRoom);

    @Query(value = "SELECT * FROM TBL_ROOM WHERE ID_ROOM = ?1 OR TYPE_ROOM = ?2 ",nativeQuery = true)
    public List<TblRoom> getDataRoomByParam(String idRoom, String typeRoom);

    @Query(value = "SELECT * FROM TBL_ROOM TR WHERE NOT EXISTS (SELECT TT.ID_ROOM FROM TBL_TRANSACTION TT WHERE TR.ID_ROOM=TT.ID_ROOM AND TT.STATUS IS NULL)", nativeQuery = true)
    public List<TblRoom> getDataTransactionByAvailableRooms();

}
