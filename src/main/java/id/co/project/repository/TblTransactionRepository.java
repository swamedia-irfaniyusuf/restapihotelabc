package id.co.project.repository;

import id.co.project.model.TblRoom;
import id.co.project.model.TblTransaction;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TblTransactionRepository extends CrudRepository<TblTransaction, String> {

    @Modifying
    @Query(value = "INSERT INTO TBL_TRANSACTION (ID_TRX,ID_CUST,ID_ROOM,TANGGAL_DAFTAR,LAMA_MENGINAP,CREATED_DATE,TOTAL_TARIF) VALUES (:idTrx, :idCust, :idRoom, :tanggalDaftar, :lamaMenginap, SYSDATE, :totalTarif)", nativeQuery = true)
    public int saveTrx(@Param("idTrx") String idTrx,
                       @Param("idCust") String idCust,
                       @Param("idRoom") String idRoom,
                       @Param("tanggalDaftar") Date tanggalDaftar,
                       @Param("lamaMenginap") int lamaMenginap,
                       @Param("totalTarif") int totalTarif);

    @Modifying
    @Query(value = "UPDATE TBL_TRANSACTION SET STATUS = 0, MODIFIED_DATE = SYSDATE WHERE ID_TRX = :idTrx ",nativeQuery = true)
    public int checkout(@Param("idTrx") String idTrx);

    @Modifying
    @Query(value = "UPDATE TBL_TRANSACTION SET STATUS = 0, MODIFIED_DATE = SYSDATE WHERE ID_CUST = :idCust ",nativeQuery = true)
    public int checkoutByCust(@Param("idCust") String idCust);

    @Query(value = "SELECT * FROM TBL_TRANSACTION WHERE STATUS IS NULL ", nativeQuery = true)
    public List<TblTransaction> getDataTransactionAll();

    @Query(value = "SELECT * FROM TBL_TRANSACTION WHERE STATUS IS NULL AND ID_TRX = ?1", nativeQuery = true)
    public TblTransaction getDataTransactionByIdTrx(String idTrx);

    @Query(value = "SELECT * FROM TBL_TRANSACTION WHERE STATUS IS NULL AND ID_ROOM = ?1", nativeQuery = true)
    public TblTransaction getDataTransactionByIdRoom(String idRoom);

    @Query(value = "SELECT * FROM TBL_TRANSACTION WHERE STATUS IS NULL AND ID_CUST = ?1", nativeQuery = true)
    public List<TblTransaction> getDataTransactionByIdCust(String idCust);



}
